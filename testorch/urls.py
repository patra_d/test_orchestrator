from django.conf.urls import url
from testorch import views

urlpatterns = [
    url(r'^run_test$', views.run_test)
]
