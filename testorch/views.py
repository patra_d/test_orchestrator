from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser

import logging

from testorch.conf import conf_settings

log_file_path = conf_settings['LOG_FILE_PATH']
logging.basicConfig(filename=log_file_path, format='%(asctime)s %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p', level=logging.DEBUG)

@csrf_exempt
@api_view(['POST','GET'])
def run_test(request):
	req_data = JSONParser().parse(request)
	logging.debug(req_data)
	return HttpResponse(status=200)